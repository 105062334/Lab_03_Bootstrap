# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105062334
- [ ] Name : 沈桓有
- [ ] URL : https://getbootstrap.com/docs/4.0/examples/blog/

## 10 Bootstrap elements (list in here)
1. <a class="text-muted" href="https://www.facebook.com/benson.shen.714">斗內給我</a>
2. <a class="blog-header-logo text-dark" href="https://www.techbang.com/posts/36056-ultra-king-of-thieves-1-4-turn-system-with-a-new-archive-point">盜賊系職業</a>
3. <a class="p-2 text-muted" href="https://forum.gamer.com.tw/C.php?bsn=7650&snA=925112">夜使者</a>
4. <h1 class="display-4 font-italic">盜賊系各職介紹</h1>
5. <p class="lead my-3">想要知道最詳細的內容 可以點上方連結進來看唷</p>
6. <p class="lead mb-0"><a href="https://forum.gamer.com.tw/C.php?bsn=7650&snA=935239" class="text-white font-weight-bold">More: 練等推薦地圖...</a></p>
7. <strong class="d-inline-block mb-2 text-primary">大事記</strong>
8. <div class="mb-1 text-muted">12/31</div>
9. <p class="card-text mb-auto">Big Bang大改版</p>
10. <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark" style="background: purple !important">
